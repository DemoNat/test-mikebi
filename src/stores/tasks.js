import { defineStore } from 'pinia'
import axios from 'redaxios';

export const useTasksStore = defineStore('tasks', {
  state: () => {
    return {
      userTasks: []
    }
  },
  actions: {
    getTasks() {
      axios.get('/mocData/mocData.json')
      .then((response) => this.userTasks = response.data)
      .catch((error) => console.log(error));
    }
  },
})
